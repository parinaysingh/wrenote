var app = angular.module("notesApp", []);
app.controller("notesController", function ($scope, $http) {

    function clear() {
        $scope.heading = "";
        $scope.content = "";
        $scope.form.heading.$touched = false;
        $scope.form.content.$touched = false;
    }

    // $http.get('http://localhost/html/check.php').then(function (response) {
    //     savedNotes = response.data ? response.data : [];
    //     console.log(savedNotes);
    // })

    var savedNotes = localStorage.getItem('notes');
    $scope.allNotes = savedNotes ? JSON.parse(savedNotes) : [];
    $scope.addNote = function () {
        if(!validate()){
            return;
        }
         var note = {
            heading: $scope.heading,
            content: $scope.content,
            createdAt: Date.now()
        };
        $scope.allNotes.push(note);
        $('#myModal').modal('hide');
        clear();
        localStorage.setItem('notes', (angular.toJson($scope.allNotes)));
    };

    $scope.deleteNote = function() {
        $scope.allNotes.splice(this.$index, 1);
        localStorage.setItem('notes', angular.toJson($scope.allNotes));
    };

    $scope.edit = function () {
        event.currentTarget.setAttribute('contenteditable', true);
    };

    $scope.saveEdit = function (target) {
        event.currentTarget.setAttribute('contenteditable', false);
        if(target === 'heading'){
            this.note.heading = event.currentTarget.innerText;
        }else {
            this.note.content = event.currentTarget.innerText;
        }
        localStorage.setItem('notes', angular.toJson($scope.allNotes));
    };

    $scope.time = function () {
        var seconds =  (Date.now() - this.note.createdAt) /1000;
        var interval = Math.floor(seconds / 31536000);
        if (interval >= 1) {
            return interval + " yrs";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval >= 1) {
            return interval + " months";
        }
        interval = Math.floor(seconds / 86400);
        if (interval >= 1) {
            return interval + " days";
        }
        interval = Math.floor(seconds / 3600);
        if (interval >= 1) {
            return interval + " hrs";
        }
        interval = Math.floor(seconds / 60);
        if (interval >= 1) {
            return interval + " min";
        }
        return "Just Now";
    };

    $scope.addNoteModal = function () {
        $scope.toEditNote = undefined;
        clear();
    };

    $scope.editNoteModal = function () {
        $scope.toEditNote = this.note;
        $scope.heading = this.note.heading;
        $scope.content = this.note.content;
    };

    function validate() {
        $scope.form.heading.$touched = $scope.form.content.$touched = true;
        if ($scope.heading == "" || $scope.content == "" || $scope.heading == undefined || $scope.content == undefined){
            return false;
        }else {
            return true;
        }
    }

    $scope.saveEditedNote = function () {
        if(!validate()){
            return;
        }
        $scope.toEditNote.heading = $scope.heading;
        $scope.toEditNote.content = $scope.content;
        $('#myModal').modal('hide');
        $scope.toEditNote = undefined;
        clear();
        localStorage.setItem('notes', (angular.toJson($scope.allNotes)));
    };
    $scope.check = { index: "checktest" };
})

app.directive('notesDir', function () {
    return{
        restrict: "E",
        scope: {
            notes: '=',
            time: '=',
            deletenote: '=',
            editnotemodal: '=',
            saveedit: '=',
            edit: '='
        },
        templateUrl: "note.html"

    }
})